package org.qy.ca.client;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyStore;

/**
 * @Description:	des
 * @Author:		方宇康
 * @CreateDate:	2020/5/18 10:35
 * @Version:      	1.0.0.1
 * @Company:       联通智网科技有限公司
 */
public class HttpsTruststoreConnect
{
    /**
     * 客户端证书路径，用了本地绝对路径，需要修改
     */
    private final static String CLIENT_CERT_FILE = "E:\\个人晋升\\CA证书\\client.p12";

    /**
     * 客户端证书密码
     */
    private final static String CLIENT_PWD = "123456";

    /**
     * 信任库路径，即keytool生成的那个自定义名称的库文件
     */
    private final static String TRUST_STRORE_FILE = "D:\\Java\\jdk1.8.0_191\\jre\\lib\\security\\javaclient.truststore";

    /**
     * 信任库密码，即keytool时的密码
     */
    private final static String TRUST_STORE_PWD = "123456";

    private static String readResponseBody(InputStream inputStream) throws IOException
    {
        try
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            StringBuffer sb = new StringBuffer();
            String buff;
            while ((buff = br.readLine()) != null) {
                sb.append(buff + "\n");
            }
            return sb.toString();
        }
        finally {
            inputStream.close();
        }
    }

    public static void httpsCall() throws Exception
    {
        // 初始化密钥库
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
        KeyStore keyStore = getKeyStore(CLIENT_CERT_FILE, CLIENT_PWD, "PKCS12");
        keyManagerFactory.init(keyStore, CLIENT_PWD.toCharArray());
        // 初始化信任库
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
        KeyStore trustkeyStore = getKeyStore(TRUST_STRORE_FILE, TRUST_STORE_PWD, "JKS");
        trustManagerFactory.init(trustkeyStore);
        // 初始化SSL上下文
        SSLContext ctx = SSLContext.getInstance("SSL");
        ctx.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);
        SSLSocketFactory sf = ctx.getSocketFactory();
        HttpsURLConnection.setDefaultSSLSocketFactory(sf);
        String url = "https://www.qy-bb.club";
        URL urlObj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) urlObj.openConnection();
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 " +
                "(KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        con.setRequestProperty("Accept-Language", "zh-CN;en-US,en;q=0.5");
        con.setRequestMethod("GET");
        String res = readResponseBody(con.getInputStream());
        System.out.println(res);
    }

    /**
     * 获得KeyStore
     * 
     * @param keyStorePath
     * @param password
     * @param type
     * @return      
     * @exception   
     * @date        2020/5/18 12:18
     */
    private static KeyStore getKeyStore(String keyStorePath, String password, String type) throws Exception
    {
        FileInputStream is = new FileInputStream(keyStorePath);
        KeyStore ks = KeyStore.getInstance(type);
        ks.load(is, password.toCharArray());
        is.close();
        return ks;
    }

    public static void main(String[] args) throws Exception
    {
        httpsCall();
    }

}
